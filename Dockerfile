FROM mcr.microsoft.com/azure-cli

RUN apk add --no-cache git bash curl
RUN curl -O https://download.visualstudio.microsoft.com/download/pr/254b4820-7917-4248-b353-a6350020be96/19ceb70a7b825f866761c4e2ae0d6d3f/dotnet-sdk-3.1.415-linux-musl-x64.tar.gz \
    && mkdir -p /usr/bin/dotnet \
    && tar -xzvf dotnet-sdk-3.1.415-linux-musl-x64.tar.gz -C /usr/bin/dotnet \
    && rm -rf \
        dotnet-sdk-3.1.415-linux-musl-x64.tar.gz \
        /tmp/* \
        /var/cache/apk/*

# Install docker cli
RUN apk add --no-cache --update docker openrc
RUN rc-update add docker boot

# Install docker-compose
RUN curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose \
    && chmod +x /usr/local/bin/docker-compose

# Export dotnet command
ENV PATH=$PATH:/usr/bin/dotnet